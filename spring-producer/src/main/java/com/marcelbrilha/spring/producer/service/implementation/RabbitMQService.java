package com.marcelbrilha.spring.producer.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marcelbrilha.spring.producer.amqp.AmqpProducer;
import com.marcelbrilha.spring.producer.dto.Message;
import com.marcelbrilha.spring.producer.service.AmqpService;

@Service
public class RabbitMQService implements AmqpService {

	@Autowired
	private AmqpProducer<Message> amqp;

	@Override
	public void sendToConsumer(Message message) {
		amqp.producer(message);
	}
}
