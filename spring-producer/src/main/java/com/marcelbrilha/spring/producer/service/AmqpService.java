package com.marcelbrilha.spring.producer.service;

import com.marcelbrilha.spring.producer.dto.Message;

public interface AmqpService {

	void sendToConsumer(Message message);
}
