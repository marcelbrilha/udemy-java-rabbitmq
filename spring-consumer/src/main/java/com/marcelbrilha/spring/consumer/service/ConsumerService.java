package com.marcelbrilha.spring.consumer.service;

import com.marcelbrilha.spring.consumer.dto.Message;

public interface ConsumerService {

	void action(Message message) throws Exception;
}
