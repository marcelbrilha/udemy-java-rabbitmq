package com.marcelbrilha.spring.consumer.configuration;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {

	@Bean
	public MessageConverter jsonConvert() {
		return new Jackson2JsonMessageConverter();
	}

	public SimpleRabbitListenerContainerFactory factory(ConnectionFactory connectionFactory,
			SimpleRabbitListenerContainerFactoryConfigurer configurer) {
		
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		configurer.configure(factory, connectionFactory);
		factory.setMessageConverter(jsonConvert());
		return factory;
	}
}



//@EnableRabbit
//@Configuration
//public class RabbitConfiguration {
//	
//	@Value("${spring.jms.rabbitmq.host}")
//	private String host;
//	
//	@Value("${spring.jms.rabbitmq.virtualHost}")
//	private String virtualHost;
//	
//	@Value("${spring.jms.rabbitmq.port}")
//	private int port;
//	
//	@Value("${spring.jms.rabbitmq.username}")
//	private String username;
//	
//	@Value("${spring.jms.rabbitmq.password}")
//	private String password;
//	
//	@Bean
//	public MessageConverter messageConverter() {
//		return new Jackson2JsonMessageConverter();
//	}
//	
//	@Bean
//	public ConnectionFactory connectionFactory() {
//		final CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
//		
//		connectionFactory.setHost(host);
//		connectionFactory.setVirtualHost(virtualHost);
//		connectionFactory.setPort(port);
//		connectionFactory.setUsername(username);
//		connectionFactory.setPassword(password);
//
//		return connectionFactory;
//	}
//
//	@Bean
//	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
//		return new RabbitTemplate(connectionFactory);
//	}
//
//}
