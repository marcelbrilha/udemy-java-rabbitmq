package com.marcelbrilha.spring.consumer.service.implementation;

import org.springframework.stereotype.Service;

import com.marcelbrilha.spring.consumer.dto.Message;
import com.marcelbrilha.spring.consumer.service.ConsumerService;

@Service
public class ConsumerServiceImplementation implements ConsumerService {

	@Override
	public void action(Message message) {
		System.out.println(message.getText());
	}
}
