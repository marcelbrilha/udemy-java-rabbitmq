# Udemy Java - RabbitMQ

Comando para rodar rabbitmq no docker:

```sh
$ docker run -d -p 15672:15672 -p 5672:5672 --name rabbitmq rabbitmq:3-management
```

Entrar no container para executar comandos:

```sh
$ docker exec -it rabbitmq bash 
```
